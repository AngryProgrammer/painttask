﻿using System;
using System.IO;
using UnityEngine;

public class SaveHelper : MonoBehaviour
{
    public void Save(Texture2D texture2D)
    {
        var bytes = texture2D.EncodeToPNG();

        var dirPath = Application.dataPath + "/../SaveImages/";

        if (!Directory.Exists(dirPath))
        {
            Directory.CreateDirectory(dirPath);
        }

        var tileStamp = DateTime.Now.ToString("yyyyMMddHHmmssfff");
        File.WriteAllBytes(dirPath + "Image-" + tileStamp + ".png", bytes);
    }
}
