using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace PaintTestTask
{
    public class DrawingSettings : MonoBehaviour
    {
        private float _transparency = 1f;
        private CUIColorPicker _cUIColorPicker;

        private void Start()
        {
            _cUIColorPicker = FindObjectOfType<CUIColorPicker>();
        }

        public void SetMarkerColour(Color new_color)
        {
            Drawable.PenColour = new_color;
        }
    
        public void SetMarkerWidth(int new_width)
        {
            Drawable.PenWidth = new_width;
        }
        public void SetMarkerWidth(float new_width)
        {
            SetMarkerWidth((int)new_width);
        }

        public void SetNewColor()
        {
            Color c = _cUIColorPicker.Color;
            c.a = _transparency;
            SetMarkerColour(c);
            Drawable._drawable.SetPenBrush();
        }

        public void SetEraser()
        {
            SetMarkerColour(new Color(1f, 1f, 1f, 1f));
        }

        public void PartialSetEraser()
        {
            SetMarkerColour(new Color(255f, 255f, 255f, 0.5f));
        }
    }
}